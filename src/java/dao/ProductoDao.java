package dao;

import java.util.HashMap;
import model.ProductoModel;
import model.VentaProdModel;
import sql.HSql;

/**
 * @author Escue
 */
public class ProductoDao {

    public ProductoModel[] getProducto() {
        HSql sql = new HSql();
        String strSQL = "SELECT * FROM PRODUCTO ORDER BY 2";
        HashMap[] arrQuery = sql.QuerySQL(strSQL);
        ProductoModel[] ProductoModels = new ProductoModel[arrQuery.length];
        for (int i = 0; i < arrQuery.length; i++) {
            HashMap registro = (HashMap) arrQuery[i];
            ProductoModel producto = new ProductoModel();
            producto.setIdProducto(registro.get("ID_PRODUCTO").toString());
            producto.setNombre(registro.get("NOMBRE").toString());
            ProductoModels[i] = producto;
        }
        return ProductoModels;
    }

    public ProductoModel[] getProductoById(String idProd) {
        HSql sql = new HSql();
        String strSQL = "SELECT * FROM PRODUCTO WHERE ID_PRODUCTO = '" + idProd + "';";
        HashMap[] arrQuery = sql.QuerySQL(strSQL);
        ProductoModel[] productoModels = new ProductoModel[arrQuery.length];
        for (int i = 0; i < arrQuery.length; i++) {
            HashMap registro = (HashMap) arrQuery[i];
            ProductoModel descrpcion = new ProductoModel();
            descrpcion.setIdProducto(registro.get("ID_PRODUCTO").toString());
            descrpcion.setDescripcion(registro.get("DESCRIPCION").toString());
            descrpcion.setCategoria(registro.get("CATEGORIA").toString());
            descrpcion.setStockMin(registro.get("STOCK_MIN").toString());
            descrpcion.setStockMax(registro.get("STOCK_MAX").toString());
            descrpcion.setPrecioVenta(registro.get("PRECIO_VENTAS").toString());
            descrpcion.setPrecioCompra(registro.get("PRECIO_COMPRA").toString());
            descrpcion.setCantidad(registro.get("CANTIDAD").toString());
            descrpcion.setIdProveedor(registro.get("ID_PROVEEDOR").toString());
            productoModels[i] = descrpcion;
        }
        return productoModels;
    }

    public boolean newCant(ProductoModel pm) {
        HSql sql = new HSql();
        String strSQL = "UPDATE PRODUCTO SET CANTIDAD = '" + pm.getCantidad() + "' WHERE ID_PRODUCTO = '" + pm.getIdProducto() + "';";
        return sql.ComandoConsola(strSQL);
    }
}
