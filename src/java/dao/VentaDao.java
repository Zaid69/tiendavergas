/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.HashMap;
import model.VentaModel;
import sql.HSql;

/**
 *
 * @author Escuela
 */
public class VentaDao {
    
        public VentaModel[] getVenta() {
        HSql sql = new HSql();
        String strSQL = "SELECT * FROM ventas;";
        HashMap[] arrQuery = sql.QuerySQL(strSQL);
        VentaModel[] ventaModels = new VentaModel[arrQuery.length];
        for (int i = 0; i < arrQuery.length; i++) {
            HashMap registro = (HashMap) arrQuery[i];
            VentaModel Venta = new VentaModel();
            Venta.setFolio(registro.get("FOLIO").toString());
            Venta.setFecha(registro.get("FECHA").toString());
            Venta.setTotal(registro.get("TOTAL").toString());
            Venta.setIdEmpleado(registro.get("ID_EMPLEADO").toString());
            ventaModels[i] = Venta;
        }
        return ventaModels;
    }
        
        
    public String addVenta(VentaModel vm) {
        HSql sql = new HSql();
        String folio = "";
        HashMap[] arrResultados;
        String strSQL = "INSERT INTO `ventas`(`FECHA`,`TOTAL`,`ID_EMPLEADO`) VALUE (now(),'"+vm.getTotal()+"','E-1');";
        arrResultados = sql.ExecuteSQLWithReturn(strSQL, "ventas");
        HashMap resultado = (HashMap) arrResultados[0];
        folio = (String) resultado.get("last_id");
        return folio;
    }

//    public boolean getVentaById(String folio, String RESUL, VentaModel vm) {
//        HSql sql = new HSql();
//        String strSQL = "INSERT INTO `ventas`(`FOLIO`,`FECHA`,`TOTAL`,`ID_EMPLEADO`) VALUE (" + folio + ",now()," + RESUL + ",'E-1');";
//        return sql.ComandoConsola(strSQL);
//    }
    
}
