package dao;

import java.util.HashMap;
import model.ProductoModel;
import model.VentaModel;
import model.VentaProdModel;
import sql.HSql;

/**
 * @author Escuela
 */
public class VentaProdDao {

    public VentaProdModel[] getVentasProd() {
        HSql sql = new HSql();
        String strSQL = "SELECT * FROM ventas_producto;";
        HashMap[] arrQuery = sql.QuerySQL(strSQL);
        VentaProdModel[] ventasProdModels = new VentaProdModel[arrQuery.length];
        for (int i = 0; i < arrQuery.length; i++) {
            HashMap registro = (HashMap) arrQuery[i];
            VentaProdModel VentaProd = new VentaProdModel();
            VentaProd.setIdProducto(registro.get("ID_PRODUCTO").toString());
            VentaProd.setFolio(registro.get("FOLIO").toString());
            VentaProd.setCantidadVentas(registro.get("CANTIDAD_VENTAS").toString());
            VentaProd.setSubtotal(registro.get("SUBTOTAL").toString());
            ventasProdModels[i] = VentaProd;
        }
        return ventasProdModels;
    }

    public String addVentaProd(VentaProdModel vpm) {
        HSql sql = new HSql();
        String folio = "";
        HashMap[] arrResultados;
        String strSQL = "INSERT INTO `ventas_producto`(`ID_PRODUCTO`,`CANTIDAD_VENTAS`,`SUBTOTAL`) VALUE ('" + vpm.getIdProducto() + "','" + vpm.getCantidadVentas() + "','" + vpm.getSubtotal() + "');";
        arrResultados = sql.ExecuteSQLWithReturn(strSQL, "ventas_producto");
        HashMap resultado = (HashMap) arrResultados[0];
        folio = (String) resultado.get("last_id");
        return folio;
    }
}
