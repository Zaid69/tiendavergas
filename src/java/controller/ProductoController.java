/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.ProductoDao;
import dao.VentaDao;
import dao.VentaProdDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.ProductoModel;
import model.VentaModel;
import model.VentaProdModel;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author Escuela
 */
public class ProductoController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.setContentType("application/json; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");

        PrintWriter out = response.getWriter();
        String jsonString = "";
        JSONParser parser = new JSONParser();
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if (isMultipart) {
            System.out.println("Form tipo multipart");
        }
        try {
            String apiCall = request.getParameter("ApiCall");
            if (null != request.getParameter("ApiCall")) {
                try {
                    if (apiCall.equals("addProd")) {
                        VentaProdModel vpm = new VentaProdModel();
                        vpm.setIdProducto(request.getParameter("txtId"));
                        vpm.setFolio(request.getParameter("folio"));
                        vpm.setCantidadVentas(request.getParameter("slCant"));
                        vpm.setSubtotal(request.getParameter("txtPrec"));
                        String VentaProd = new VentaProdDao().addVentaProd(vpm);
                        
                        VentaModel vm = new VentaModel();
                        vm.setTotal(request.getParameter("txtTotal"));
                        String Venta = new VentaDao().addVenta(vm);
                        
                        ProductoModel pm = new ProductoModel();
                        pm.setIdProducto(request.getParameter("txtId"));
                        pm.setCantidad(request.getParameter("txtNewCant"));
                        boolean NewCant = new ProductoDao().newCant(pm);
                        
                        response.sendRedirect("Menu.jsp?Venta" + " No" + " " + VentaProd);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
        }
//        try (PrintWriter out = response.getWriter()) {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet ProductoController</title>");            
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet ProductoController at " + request.getContextPath() + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
//        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
