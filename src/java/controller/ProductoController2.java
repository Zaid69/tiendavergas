package controller;

import dao.VentaProdDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.VentaProdModel;
import org.json.simple.parser.JSONParser;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author Escuela
 */
public class ProductoController2 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
//        response.setContentType("application/json; charset=UTF-8");
//        response.setCharacterEncoding("UTF-8");
//        request.setCharacterEncoding("UTF-8");

//        PrintWriter out = response.getWriter();
//        String jsonString = "";
//        JSONParser parser = new JSONParser();
//        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
//        if (isMultipart) {
//            System.out.println("Form tipo multipart");
//        }
//        try {
//            String apiCall = request.getParameter("ApiCall");
//            if (null != request.getParameter("ApiCall")) {
//                try {
//                    if (apiCall.equals("addProd")) {
//                        VentaProdModel vpm = new VentaProdModel();
//                        vpm.setIdProducto(request.getParameter("txtId"));
//                        vpm.setFolio(request.getParameter("folio"));
//                        vpm.setCantidadVentas(request.getParameter("txtCant"));
//                        vpm.setSubtotal(request.getParameter("txtPrec"));
//                        String addEdit = new VentaProdDao().addVentaProd(vpm);
//                        response.sendRedirect("Menu.jsp?modelId=" + vpm.getFolio() + "&res=" + addEdit + "ADD");
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//        } catch (Exception e) {
//        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
