package model;

/**
 * @author Escuela
 */
public class ProductoModel {

    String idProducto;
    String nombre;
    String Descripcion;
    String categoria;
    String stockMin;
    String stockMax;
    String precioVenta;
    String precioCompra;
    String cantidad;
    String idProveedor;

    public ProductoModel() {
        this.idProducto = "";
        this.nombre = "";
        this.Descripcion = "";
        this.categoria = "";
        this.stockMin = "";
        this.stockMax = "";
        this.precioVenta = "";
        this.precioCompra = "";
        this.cantidad = "";
        this.idProveedor = "";
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public void setStockMin(String stockMin) {
        this.stockMin = stockMin;
    }

    public void setStockMax(String stockMax) {
        this.stockMax = stockMax;
    }

    public void setPrecioVenta(String precioVenta) {
        this.precioVenta = precioVenta;
    }

    public void setPrecioCompra(String precioCompra) {
        this.precioCompra = precioCompra;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public void setIdProveedor(String idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getIdProducto() {
        return idProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public String getCategoria() {
        return categoria;
    }

    public String getStockMin() {
        return stockMin;
    }

    public String getStockMax() {
        return stockMax;
    }

    public String getPrecioVenta() {
        return precioVenta;
    }

    public String getPrecioCompra() {
        return precioCompra;
    }

    public String getCantidad() {
        return cantidad;
    }

    public String getIdProveedor() {
        return idProveedor;
    }

}
