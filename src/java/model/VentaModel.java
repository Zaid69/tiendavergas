package model;

/**
 * @author Escuela
 */
public class VentaModel {

    String folio;
    String fecha;
    String total;
    String idEmpleado;

    public VentaModel() {
        this.folio = "";
        this.fecha = "";
        this.total = "";
        this.idEmpleado = "";
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(String idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

}
