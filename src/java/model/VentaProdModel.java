package model;

/**
 * @author Escuela
 */
public class VentaProdModel {

    String idProducto;
    String folio;
    String cantidadVentas;
    String subtotal;

    public VentaProdModel() {
        this.idProducto = "";
        this.folio = "";
        this.cantidadVentas = "";
        this.subtotal = "";
    }

    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getCantidadVentas() {
        return cantidadVentas;
    }

    public void setCantidadVentas(String cantidadVentas) {
        this.cantidadVentas = cantidadVentas;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

}
