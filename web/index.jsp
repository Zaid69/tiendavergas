<%-- 
    Document   : index
    Created on : 14/07/2019, 10:07:14 PM
    Author     : Zaid
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tienda</title>
        <!--Bootstrap-->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!--jQuery-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <!--css-->
        <link rel="stylesheet" href="css/style.css">
        <!--js-->
        <script src="tuEnlace.js"></script>
        
    </head>
    <body>
        <h1>Hello World!</h1>
        <!-- Bootstrap core JavaScript -->
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    </body>
</html>
