<%-- 
    Document   : menu
    Created on : 10/08/2019, 10:27:44 AM
    Author     : Escuela
--%>

<%@page import="controller.ProductoController"%>
<%@page import="controller.AjaxController"%>
<%@page import="model.ProductoModel"%>
<%@page import="model.VentaModel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        ProductoModel[] descs = new AjaxController().getProductos();
    %>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>MENU</title>
        <!--Bootstrap-->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!--jQuery-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <!--css-->
        <link rel="stylesheet" href="css/style.css">
        <!--js-->
        <script src="js/Menu.js"></script>

    </head>

    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-4">

                </div>
                <div class="col-8">
                    <h1><b>EL CHIPOCLE ENMASCARADO</b></h1>
                </div>
            </div>

            <!-- FORMULARIO -->
            <form action="ProductoController">
                <div class="row">
                    <img src="img/imgTienda.png" alt="Tienda" class="col-4">
                    <div class="col-8 bg-primary text-white rounded">
                        <div class="row mt-5 mb-4">
                            <div class="col-8">
                                <label for="">Nombre del Producto</label><br>
                                <select class="custom-select" name="slProd" id="slProd" onchange="getDesc(this.value)">
                                    <option value="0">Producto</option>
                                    <%for (int cProd = 0; cProd < descs.length; cProd++) {%>
                                    <option value="<%=descs[cProd].getIdProducto()%>">
                                        <%=descs[cProd].getNombre()%>
                                    </option>
                                    <%}%>
                                </select>
                            </div>
                            <div class="col-2">
                                <label>Ventas</label><br>
                                <select name="slCant" id="slCant" class="custom-select" onchange="Mult()">
                                    <option value="0">Ventas</option>
                                    <%for (int cProd = 1; cProd < 457; cProd++) {%>
                                    <option value="<%=cProd%>">
                                        <%=cProd%>
                                    </option>
                                    <%}%>
                                </select>
                            </div>
                            <div class="col-2">
                                <label>Precio</label><br>
                                <input name="txtPrec" id="txtPrec" class="form-control" type="text" readonly="readonly" >
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-8">
                                <label>Descripcion del Producto</label><br>
                                <input id="txtDesc" class="form-control" type="text">
                            </div>
                            <div class="col-2">
                                <label>Cantidad</label><br>
                                <input name="txtCant" id="txtCant" class="form-control" type="text">
                            </div>
                            <div class="col-2">
                                <label>Total</label><br>
                                <input name="txtTotal" id="txtTotal" class="form-control" type="text">
                            </div>
                            <div class="col-2 d-none">
                                <label>ID del Producto</label><br>
                                <input name="txtId" id="txtId" class="form-control" type="text">
                            </div>
                            <div class="col-2 d-none">
                                <label>Nueva Cantidad</label><br>
                                <input name="txtNewCant" id="txtNewCant" class="form-control" type="text">
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-12">
                                <input type="hidden" name="ApiCall" value="addProd">
                                <button class="btn btn-success btn-lg btn-block">Vender</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <!-- Bootstrap core JavaScript -->
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    </body>

</html>